<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://antimatter/antimatter.yaml',
    'modified' => 1502863690,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => false
        ]
    ]
];

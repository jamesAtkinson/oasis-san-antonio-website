<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://problems/problems.yaml',
    'modified' => 1502863690,
    'data' => [
        'enabled' => true,
        'built_in_css' => true
    ]
];

<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/oasis-san-antonio/user/accounts/admin.yaml',
    'modified' => 1504451917,
    'data' => [
        'email' => 'jamesatkinson96@gmail.com',
        'fullname' => 'James Atkinson',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$xCImCFPdirNGlavoYdbwZedLOCAIeayzjWL5Cv2T7luz5fiufzm8K'
    ]
];

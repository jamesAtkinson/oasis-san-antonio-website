<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://gateway/gateway.yaml',
    'modified' => 1504453092,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => true
        ]
    ]
];

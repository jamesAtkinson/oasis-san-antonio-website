<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Applications/MAMP/htdocs/oasis-san-antonio/user/plugins/admin/blueprints/admin/pages/move.yaml',
    'modified' => 1502863690,
    'data' => [
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'route' => [
                    'type' => 'parents',
                    'label' => 'PLUGIN_ADMIN.PARENT',
                    'classes' => 'fancy'
                ]
            ]
        ]
    ]
];
